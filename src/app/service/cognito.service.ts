/**
 * Created by Emmanuel Nartey
 */

export interface CognitoCallback {
    cognitoCallback(message: string, result: any): void;
}
