import { Component, OnInit, OnDestroy } from '@angular/core';
import {AuthorizationService} from "../shared/authorization.service";
import { ActivatedRoute, Router } from '@angular/router';
import {CognitoCallback} from "../service/cognito.service";

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotPasswordComponent implements CognitoCallback{
  email: string;
  errorMessage: string;

  constructor(private auth: AuthorizationService,
              private _router: Router) { 
                this.errorMessage = null;
  }

  onNext() {
    this.errorMessage = null;
    this.auth.forgotPassword(this.email, this);
  }

  cognitoCallback(message: string, result: any) {
      if (message == null && result == null) { //error
          this._router.navigate(['/forgot-password', this.email]);
      } else { //success
          this.errorMessage = message;
      }
  }

}

@Component({
  selector: 'app-forgot-step-2',
  templateUrl: './forgotStep2.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotPasswordStep2Component implements CognitoCallback, OnInit, OnDestroy {
  verificationCode: string;
    email: string;
    password: string;
    errorMessage: string;
    private sub: any;

  constructor(private auth: AuthorizationService,
              private _router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.email = params['email'];
    });
    this.errorMessage = null;
  }

  ngOnDestroy() {
      this.sub.unsubscribe();
  }

  onSubmit() {
      this.errorMessage = null;
      this.auth.confirmNewPassword(this.email, this.verificationCode, this.password, this);
  }

  cognitoCallback(message: string) {
      if (message != null) { //error
          this.errorMessage = message;
          console.log("result: " + this.errorMessage);
      } else { //success
          this._router.navigate(['/login']);
      }
  }

}
